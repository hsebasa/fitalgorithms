__author__ = 'Math Decision'

from scipy.optimize import minimize
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
import random




__indexColor = 0
__color_dict = ['blue', 'green', 'red', 'yellow', 'cyan', 'ff69b4', 'purple',
'magenta', '#faebd7', '#2e8b57', '#eeefff', '#da70d6', '#ff7f50', '#cd853f',
'#bc8f8f', '#5f9ea0', '#daa520', 'white']


#----------------------------------------------
#Nota: en este programa se encuentra una
#aproximacion a una funcion f, dada una serie de
#datos
#Las funciones deben tener dos parametros: t, a
#   t es la lista de elementos a evaluar la funcion
#   a son los argumentos de los que depende la funcion

#Por defecto, vamos a tomar
#       f(t)=Aexp(lambda*t)+Bcos(k(t-t0))
#El parametro 'a' pedido en las funciones de este
#modulo representa las variables de la funcion f
#como sigue:
#a[0]=A
#a[1]=lambda
#a[2]=B
#a[3]=k
#a[4]=t0
#Como el parametro t y x son dados, son
#representados como variables globales.
#es por eso que las funciones f, qf, l1f, minl1f
#solo tienen como parametro la variable 'a' que
#es lo que vamos a determinar
#----------------------------------------------



#El numero de argumentos debe ser finito y mayor que cero.
# f=lambda t, a: a[0]*np.exp(a[1]*t)+a[2]*np.sin(a[3]*(t-a[4]*np.ones(len(t))))
def bestfit2(t, x, f, nArgs=-1, reps=52, method='l2', amin='NaN', amax='NaN'):
    """
        Esta funcion halla los argumentos bajo los cuales la funcion f especificada, se aproxima a x(t).
        Como funcion por defecto, se toma
               f(t)=Aexp(lambda*t)+Bcos(k(t-t0))
        Esto solo halla el argumento  en un intervalo especificado en (amin, amax), bajo el cual la distancia de f a x
        es minima.
        Reps corresponde al numero de repeticiones con inicializacion aleatoria.
        Mientras mas grande sea el valor de Reps, mas preciso sera encontrar este minimo en el intervalo dado.
        Se debe especificar el numero de argumentos cuando se define una funcion diferente a la usual.

        En method se especifica cual de las dos distancias se usara: si la de minimos cuadrados o la de suma de valores absoluto.
        Nota: Es mas recomendable, cuando la funcion es por defecto, que se utilicen
        amin=['NaN',-10,'NaN',-1,-1],amax=['NaN',20,'NaN',1,1]
    """
    if nArgs < 0:
        nArgs = __gnArgs(f)
        if type(nArgs) == str: return 'Error al estimar el numero de argumentos de la funcion g(a) = f(t, a)'

    # TODO: Adivinar el numero de argumentos con un try


    #Se establecen valores por defecto dado el caso.
    if amin == 'NaN':
        amin = [-10 for i in range(nArgs)]
    if amax == 'NaN':
        amax = [10 for i in range(nArgs)]

    #Valor que retorna en el peor de los casos
    minimum = [(amax[i] - amin[i]) / 2 for i in range(nArgs)]

    #'QF' es la distancia cuadratica, en cualquier otro caso, es la distancia de suma de absolutos.
    if method == 'l2':
        err = lambda args: sum((np.array(f(t, args)) - x)**2)
    elif method == 'l1':
        err = lambda args: sum(abs(np.array(f(t, args)) - x))
    else:
        print 'Metodo no especificado'
        exit()

    minimo = err(minimum)

    #Aqui se evaluan varios valores aleatorios del intervalo y se obtiene el menor de todos esos valores
    for i in range(reps):
        a = [(amax[i] - amin[i]) * random.random() + amin[i] for i in range(nArgs)]
        loc = minimize(err, a, method='nelder-mead').x
        ste = err(loc)
        if minimo > ste:
            minimum = loc
            minimo = ste

    return minimum






#Devuelve el numero de argumentos de la funcion f
def __gnArgs(f):
    a = []
    while True:
        try:
            f(0, a)
            return len(a)

        except IndexError:
            a.append(0)

        except:
            return 'Error'





def graficar(t, x, f, s):
    """

    :param t:
    :param x: Parametro que depende del tiempo. Hace parte de los puntos dados.
    :param f: Funcion a la cual se aproximan los puntos. f(t,a) debe ser escrita segun como se requiere en la Nota inicial
    :param s: Se graficara la funcion f(t, s)
    """
    plt.plot(t, x, 'o')
    plt.plot(t, f(t, s))
    plt.show()








def gDelta(t, f, s, ds, nitidez = 2, placeInfo = [0, 0]):
    """

    :param t:
    :param x: Parametro que depende del tiempo. Hace parte de los puntos dados.
    :param f: Funcion a la cual se aproximan los puntos. f(t,a) debe ser escrita segun como se requiere en la Nota inicial
    :param s: Se graficara la funcion f(t, s)
    :param nitidez: numero de lineas que dibuja para cada parametro. Si se especifican dos, mostrara solo la menor y mayor
                    linea correspondientes.
    """
    global __indexColor
    #placeInfo[1] += max([f(k, s) for k in t]) + len(s) + placeInfo[1] + 3
    #placeInfo[0] += min(t)
    g = []


    for a in range(len(s)):
        if ds[a] == 0 or set(ds[a]) == {0}: continue
        intl, = plt.plot(t, f(t, list(np.array(s) + ds[a][0] * np.identity(len(s))[a])), color = __color_dict[__indexColor % len(__color_dict)], label = 'Param. ' + str(a))
        g += [intl]
        __indexColor += 1



    __indexColor = 0
    for a in range(len(s)):
        if ds[a] == 0 or set(ds[a]) == {0}: continue
        for e in np.linspace(ds[a][0], ds[a][1], nitidez):
            I = e * np.identity(len(s))[a]
            u = list(np.array(s) + I)

            if e != 0:
                alph = abs((ds[a][0] - e)/(ds[a][1] - ds[a][0]))
            else:
                alph = 0
            if alph < 0.01: alph = 1
            if e != ds[a][0]:
                plt.plot(t, f(t, u), alpha = alph*(0.5), color = __color_dict[__indexColor % len(__color_dict)])


        #plt.text(placeInfo[0], placeInfo[1] - a, 'Color del param. ' + str(a), color = __color_dict[__indexColor % len(__color_dict)])

        __indexColor += 1

    plt.plot(t, f(t, s), color = 'black')
    plt.legend(g)
    plt.show()




def gAleat(t, x, f, dx, nTryes, reps=52, method='l2', amin='NaN', amax='NaN'):
    global __indexColor
    s1 = bestfit2(t, x, f, reps = reps, method = method, amin = amin, amax = amax)
    plt.plot(t, f(t, s1), label = 'Funcion antes', color = __color_dict[__indexColor % len(__color_dict)])
    plt.plot(t, x, 'o', label = 'Antes', color = __color_dict[__indexColor % len(__color_dict)])

    for i in range(nTryes):
        __indexColor += 1
        indt = random.randrange(len(t))
        sgn =  (-1)**random.randrange(2)

        y = list(x)
        y[indt] += sgn*dx

        s2 = bestfit2(t, y, f, reps = reps, method = method, amin = amin, amax = amax)



        p3, = plt.plot([t[indt]], [y[indt]], 'o', label = 'Despues', alpha = 0.5,  color = __color_dict[__indexColor % len(__color_dict)])
        p4, = plt.plot(t, f(t, s2), label = 'Funcion despues', color = __color_dict[__indexColor % len(__color_dict)])
    #plt.legend([p1, p2, p3, p4])
    plt.show()








#Esto hace lo mismo, solo que carga el archivo arch.
def bestfit2_load(arch,f=lambda t,a: a[0]*np.exp(a[1]*t)+a[2]*np.sin(a[3]*(t-a[4]*np.ones(len(t)))),nArgs=6, reps=52,method='QF',amin='NaN',amax='NaN'):
    df = pd.read_csv(arch)

    colfecha = 'fecha'
    colvalor = 'valor'
    formato_fecha = '%Y/%m/%d'

    df[colfecha] = pd.to_datetime(df[colfecha], format=formato_fecha)
    df[colvalor] = df[colvalor].astype(float)

    fecha_base = datetime.datetime(2013,1,1)


    t1 = (df[colfecha].apply(lambda x: float((x-fecha_base).days))).values
    x1 = df[colvalor].values
    return bestfit2(t1,x1,f,nArgs,reps,method,amin,amax)




if __name__ == '__main__':
    #funcion por defecto.
    def __f(t, a):
        """
            Funcion f a la cual vamos a aproximar el arreglo x,t
        """
        # return a[0] * np.exp(a[1] * __t) + a[2] * np.sin(a[3] * (__t - a[4] * np.ones(len(__t))))
        return a[0] * np.exp(a[1] * t) + a[2] * np.sin(a[3] * t)

    #Ejemplo de uso de la funcion
    __t = np.linspace(0, 10, 10)
    __x = __f(__t, [1, 0.001, 20, 1])

    #s = bestfit2(__t, __x, __f, reps=200)
    #print s
    #s=bestfit_load('tst.csv')

    #graficar(__t, __x, __f, s)
    gDelta(__t, __f, [1, 0.001, 20, 1], [[-2, 2], 0, 0, 0], nitidez=2)
    gAleat(__t, __x, __f, 10, 2)
    plt.show()


