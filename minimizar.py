from scipy.optimize import minimize
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
import random
import math

#----------------------------------------------
#Nota: en este programa se encuentra una
#aproximacion a la funcion f, dada una serie de
#datos, donde
#       f(t)=Aexp(lambda*t)+Bcos(k(t-t0))
#El parametro 'a' pedido en las funciones de este
#modulo representa las variables de la funcion f
#como sigue:
#a[0]=A
#a[1]=lambda
#a[2]=B
#a[3]=k
#a[4]=t0
#Como el parametro t y x son dados, son
#representados como variables globales.
#es por eso que las funciones f, qf, l1f, minl1f
#solo tienen como parametro la variable 'a' que
#es lo que vamos a determinar
#----------------------------------------------

__t=np.array([])
__x=np.array([])

def __f(a):
    """
        Funcion f a la cual vamos a aproximar el arreglo x,t
    """
    return a[0]*np.exp(a[1]*__t)+a[2]*np.sin(a[3]*(__t-a[4]*np.ones(len(__t))))



#Esta es la funcion de distancia cuadratica. Mide la distancia (cuadratica) de los datos dados a la funcion f(a)
def __qf(a):
    """
        Funcion de distancia para la suma cuadratica
    """
    return sum(np.array(__f(a)-__x)**2)

#Aqui se minimiza la funcion qf para los datos dados t1 y x1. Devuelve un arreglo de parametros que minimizan la funcion qf
#Esta funcion utiliza minimize de la libreria scipy. Como tal funcion (minimize) es solo para minimos locales,
#pues hay que especificar un valor inicial x0, __minQF (similarmente __minL1F), retorna el menor de los parametros
#para la funcion minimize aleatoriamente en un intervalo dado [amin,amax]
def __minQF(t1,x1,amin=['NaN',-10,'NaN',-1,-1],amax=['NaN',20,'NaN',1,1],repeticiones=40):
    """
        Devuelve los parametros para los cuales la funcion QF es minima en el
        intervalo [amin, amax]. Mientras mas grande sea el valor de repeticiones
        mayor sera la presicion.
    """

    #Esta es la aproximacion por defecto que tendra si no se especifica ninguna
    if amin[0]=='NaN':
        amin[0]=min(x1)
        amin[2]=min(x1)

    if amax[0]=='NaN':
        amax[0]=max(x1)
        amax[2]=max(x1)

    global __t
    global __x

    __t=np.array(t1)
    __x=np.array(x1)

    minimum=[(amax[0]-amin[0])/2,(amax[1]-amin[1])/2,(amax[2]-amin[2])/2,(amax[3]-amin[3])/2,(amax[4]-amin[4])/2]
    minimo=__qf(minimum)

    #Aqui se evaluan varios valores aleatorios del intervalo y se obtiene el menor de todos esos valores
    for i in range(0,repeticiones,1):
        a=[(amax[0]-amin[0])*random.random()+amin[0],(amax[1]-amin[1])*random.random()+amin[1],(amax[2]-amin[2])*random.random()+amin[2],(amax[3]-amin[3])*random.random()+amin[3],(amax[4]-amin[4])*random.random()+amin[4]]
        loc=minimize(__qf, a, method='nelder-mead').x
        ste=__l1f(loc)
        if minimo>__qf(loc):
            minimum=loc
            minimo=ste
    return minimum





def __minQF_Load(arch,amin=['NaN',-10,'NaN',-1,-1],amax=['NaN',20,'NaN',1,1],repeticiones=40):
    df = pd.read_csv(arch)

    colfecha = 'fecha'
    colvalor = 'valor'
    formato_fecha = '%Y/%m/%d'

    df[colfecha] = pd.to_datetime(df[colfecha], format=formato_fecha)
    df[colvalor] = df[colvalor].astype(float)

    fecha_base = datetime.datetime(2013,1,1)


    t1 = (df[colfecha].apply(lambda x: float((x-fecha_base).days))).values
    x1 = df[colvalor].values

    return __minQF(t1,x1,amin,amax,repeticiones)



def __l1f(a):
    return sum(abs(np.array(__f(a)-__x)))

def __minL1F(t1,x1,amin=['NaN',-10,'NaN',-1,-1],amax=['NaN',20,'NaN',1,1],repeticiones=40):
    global __t
    global __x

    if amin[0]=='NaN':
        amin[0]=min(x1)
        amin[2]=min(x1)

    if amax[0]=='NaN':
        amax[0]=max(x1)
        amax[2]=max(x1)

    __t=np.array(t1)
    __x=np.array(x1)

    minimum=[(amax[0]-amin[0])/2,(amax[1]-amin[1])/2,(amax[2]-amin[2])/2,(amax[3]-amin[3])/2,(amax[4]-amin[4])/2]
    minimo=__l1f(minimum)
    for i in range(0,repeticiones,1):
        a=[(amax[0]-amin[0])*random.random()+amin[0],(amax[1]-amin[1])*random.random()+amin[1],(amax[2]-amin[2])*random.random()+amin[2],(amax[3]-amin[3])*random.random()+amin[3],(amax[4]-amin[4])*random.random()+amin[4]]
        loc=minimize(__qf, a, method='nelder-mead').x
        ste=__l1f(loc)
        if minimo>__l1f(loc):
            minimum=loc
            minimo=ste
    return minimum



def __minL1F_Load(arch,amin=['NaN',-10,'NaN',-1,-1],amax=['NaN',20,'NaN',1,1],repeticiones=40):
    df = pd.read_csv(arch)

    colfecha = 'fecha'
    colvalor = 'valor'
    formato_fecha = '%Y/%m/%d'

    df[colfecha] = pd.to_datetime(df[colfecha], format=formato_fecha)
    df[colvalor] = df[colvalor].astype(float)

    fecha_base = datetime.datetime(2013,1,1)


    t1 = (df[colfecha].apply(lambda x: float((x-fecha_base).days))).values
    x1 = df[colvalor].values

    return __minL1F(t1,x1,amin,amax,repeticiones)

















def bestfit(t,x,repeticiones=40,method='QF',amin=['NaN',-10,'NaN',-1,-1],amax=['NaN',20,'NaN',1,1]):
    if method=='QF':
        return __minQF(t,x,amin,amax,repeticiones)

    else:
        return __minL1F(t,x,amin,amax,repeticiones)




def bestfit_load(arch,repeticiones=40,method='QF',amin=['NaN',-10,'NaN',-1,-1],amax=['NaN',20,'NaN',1,1]):
    if method=='QF':
        return __minQF_Load(arch,amin,amax,repeticiones)

    else:
        return __minL1F_Load(arch,amin,amax,repeticiones)




#Comentarios
#Ejemplo de uso de la funcion
__t=np.array(range(20,300,1))
__x=__f([0,0,10,1,0])

s=bestfit(__t,__x,70)

#s=bestfit_load('tst.csv')
print(s)

plt.plot(__t,__x)
plt.plot(__t,__f(s),'o')
plt.show()

