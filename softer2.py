

import numpy as np

class softer2:

    def __k(self,x,y,C):
        return C*(np.exp(-1/2*(((1/self.__Q)*(x-y))**2)))

    def __Cqy(self,y):
        return 1/sum([self.__k(self.__x[i],y,1) for i in range(0,len(self.__x))])

    def g(self,y):
        return [self.__Cqy(j)*np.array(sum([self.__k(self.__x[i],j,1)*self.__f[i] for i in range(0,len(self.__x))]))for j in y]


    def __init__(self,x,f,Q):
        self.__x=x
        self.__Q=Q
        self.__f=f
