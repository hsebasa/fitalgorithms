import matplotlib.pyplot as plt
import numpy as np
import random
from softer2 import softer2



x=range(0,10,1)
f=[random.randrange(0,10) for i in x]
Q=0.7



s=softer2(x,f,Q)

y=np.linspace(0,10)
plt.plot(x,f)
plt.plot(y,s.g(y))
plt.show()