

import matplotlib.pyplot as plt
import numpy as np
import random

__x=np.array([])
__f=np.array([])
__Q=1

def __k(x,y,C):
    return C*(np.exp(-1/2*(((1/__Q)*(x-y))**2)))

def __Cqy(y):
    return 1/sum([__k(__x[i],y,1) for i in range(0,len(__x))])

def __g(y):
    return [__Cqy(j)*np.array(sum([__k(__x[i],j,1)*__f[i] for i in range(0,len(__x))]))for j in y]


def init(x,f,Q):
    global __x
    global __Q
    global __f
    __x=x
    __Q=Q
    __f=f


__x=range(0,10,1)
__f=[random.randrange(0,10) for i in __x]
__Q=0.9

y=np.linspace(0,10)
plt.plot(__x,__f)
plt.plot(y,__g(y))
plt.show()